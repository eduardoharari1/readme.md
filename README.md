# README.md

## Requerimientos Fundamentales: MongoDB, Node.js, Express, JQUERY, css, JavaScript, Bootsrap

### MongoDB 
En este proyecto se utilizo la base de datos de MongoDB la cual es una base de datos basada en documentos, lo que facilita la operacion de CRUD dentro de la aplicación y lo hace de una manera mucho mas efectiva que en otras bases de datos.
Para la conexion a MongoDB se utilizo una URL apuntada a LocalHost de la siguiente manera.

```
var MongoClient = require("mongodb").MongoClient,
  ObjectID = require("mongodb").ObjectID;
```

```
const mongoUrl =
  "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false";
```
Posteriormente nos conectamos  on el MongoClient a esa url de la siguiente manera.

```
  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
```

### Node

### Express
Se utilizo express como BACK-END de nuestra aplicacion el cual esta basado en node.Js

# Instalación

# Archivos

### - frontend
  - css 
    - main.css
  - images
    - logo.png
  - Js
    - articulos.js
    - main.js
    - proveedores.js
  - templates
    - articulo_form.html
    - index.html
    - proveedor_form.html
    - proveedores.html
  -package-lock.json

### - backend
- node_modules
    - todos los modulos utilizados de node
- index.js
- package-lock.json
- package.json
- router.js









